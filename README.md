#MuseumMoma

**MoMA**
*Museum of Modern Art - Android App*



*Requirements:*


*  The user must be able to see works of art related to artists.

*  The user must be able to see the name of an artist's work. And the complete detail of it by full screen.

*  The MOMA wishes that each person visiting the exhibition can take a photo and with
all the photos of the users allow to assemble a collage at the end of the year and there is a prize
for the top 3 most creative photos.

*  Login with a social network.

*  Creation of a database in Firebase, to download the detailed information
works.

*  Use of Firebase storage, to upload the photos of the contest and to download
the images of the works.



