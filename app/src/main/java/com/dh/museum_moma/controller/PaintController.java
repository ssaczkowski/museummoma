package com.dh.museum_moma.controller;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.room.Room;

import com.dh.museum_moma.communication.ServiceCallback;
import com.dh.museum_moma.communication.schema.Paint;
import com.dh.museum_moma.communication.schema.entities.EPaint;
import com.dh.museum_moma.communication.services.dao.PaintDao;
import com.dh.museum_moma.communication.services.impl.PaintRetrofitClient;
import com.dh.museum_moma.communication.services.impl.PaintRoomClient;
import com.dh.museum_moma.config.AppDatabase;
import com.dh.museum_moma.util.GeneralContext;

import java.util.ArrayList;
import java.util.List;


public class PaintController {

    PaintDao paintDao;

    public PaintController() {
        paintDao = new PaintRetrofitClient();
    }

    public void getPaint(final ServiceCallback.OnListTaskFinishListener listener) {

        if (GeneralContext.isNetConnected()) {
            paintDao.getPaints(new ServiceCallback.OnListTaskFinishListener() {
                @Override
                public void onSuccess(List dataList) {

                    ArrayList<Paint> list = (ArrayList<Paint>) dataList;
                    updatePaintDB( list);

                    listener.onSuccess(dataList);
                }

                @Override
                public void onError(String errorMsg) {
                    listener.onError(errorMsg);
                }
            });
        } else {
            PaintRoomClient paintRoomClient = new PaintRoomClient();

            AppDatabase db = Room.databaseBuilder(GeneralContext.context,AppDatabase.class, "database-museum-moma")
                    .allowMainThreadQueries().build();

            List<EPaint> ePaints = db.paintRoomDao().getAllPaints();

            List<Paint> paintList = mapEpaintToPaint(ePaints);

            listener.onSuccess(paintList);

        }

    }

    private void updatePaintDB(ArrayList<Paint> paintList) {

        for(int i=0 ; i < paintList.size(); i ++) {
            GeneralContext.getDB().paintRoomDao().insertEPaint(new EPaint(null, paintList.get(i).getName(), paintList.get(i).getImage(), paintList.get(i).getArtistId()));
        }
    }

    private List<Paint> mapEpaintToPaint(List<EPaint> ePaints) {
            List<Paint> paintList = new ArrayList<>();

            for (EPaint ePaint : ePaints) {
                paintList.add(new Paint(ePaint.getImage(),ePaint.getName(),ePaint.getArtistId()));
            }
            return paintList;
        }




}
