package com.dh.museum_moma;


import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.dh.museum_moma.ui.home.HomeActivity;
import com.dh.museum_moma.ui.login.LoginActivity;
import com.dh.museum_moma.util.GeneralContext;
import com.facebook.AccessToken;

    public class SplashActivity extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash);
            getSupportActionBar().hide();

            Animation animation = AnimationUtils.loadAnimation(this, R.anim.animation);

            ImageView imageLogo = findViewById(R.id.splash_activity_logo);

            TextView textView = findViewById(R.id.splash_activity_textView);

            imageLogo.startAnimation(animation);
            textView.startAnimation(animation);

            new Handler().postDelayed(() -> {

                GeneralContext.set(this);

                if (AccessToken.getCurrentAccessToken() == null) {
                    Intent intentLoginActivity = new Intent(this, LoginActivity.class);
                    this.startActivity(intentLoginActivity);
                    finish();
                } else {
                    Intent intentHomeActivity = new Intent(this, HomeActivity.class);
                    startActivity(intentHomeActivity);
                    finish();
                }

            }, 3000);



        }
    }

