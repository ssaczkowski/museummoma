package com.dh.museum_moma.config;

import android.content.Context;


import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.dh.museum_moma.communication.schema.entities.EArtist;
import com.dh.museum_moma.communication.schema.entities.EPaint;
import com.dh.museum_moma.communication.services.dao.ArtistRoomDao;
import com.dh.museum_moma.communication.services.dao.PaintRoomDao;


@Database(entities = {EPaint.class, EArtist.class},version = 2)
public abstract class AppDatabase extends RoomDatabase {

    public abstract PaintRoomDao paintRoomDao();

    public abstract ArtistRoomDao artistRoomDao();

    public static AppDatabase getIstanceDB(Context context){
        AppDatabase db = Room.databaseBuilder(context,AppDatabase.class, "database-museum-moma2")
                .allowMainThreadQueries().build();
        return db;
    }
}
