package com.dh.museum_moma.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.room.Room;

import com.dh.museum_moma.config.AppDatabase;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

public class GeneralContext {
    public static Context context;

    public static void set(Context ctx) {
        context = ctx.getApplicationContext();

        AppDatabase db = Room.databaseBuilder(GeneralContext.context,AppDatabase.class, "database-museum-moma2")
                .allowMainThreadQueries().build();
    }

    public static boolean isNetConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) GeneralContext.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static AppDatabase getDB() {
        return AppDatabase.getIstanceDB(GeneralContext.context);
    }

    public static String getUserId() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
}
