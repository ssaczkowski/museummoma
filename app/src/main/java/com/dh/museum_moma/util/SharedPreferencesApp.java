package com.dh.museum_moma.util;


import android.content.Context;
import android.content.SharedPreferences;

import com.dh.museum_moma.communication.schema.Message;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedPreferencesApp {

    public static final String PREFS_NAME = "MOMA_CHAT_APP";
    private static final String MESSAGES = "messages";

    public SharedPreferencesApp() {
        super();
    }

    public static void saveMessages(Context context, List<Message> messageList) {
        android.content.SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();


        Gson gson = new Gson();
        String jsonMessages = gson.toJson(messageList);

        editor.putString(MESSAGES, jsonMessages);

        editor.commit();
    }

    public static void saveMessage(Context context, Message message) {
        android.content.SharedPreferences settings;
        SharedPreferences.Editor editor;


        ArrayList<Message> messageList = getMessages(context);
        messageList.add(message);

        remove(context);

        saveMessages(context,messageList);

    }

    public static ArrayList<Message> getMessages(Context context) {
        android.content.SharedPreferences settings;
        List<Message> messages;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(MESSAGES)) {
            String jsonFavorites = settings.getString(MESSAGES, null);
            Gson gson = new Gson();
            Message[] messagesItems = gson.fromJson(jsonFavorites,
                    Message[].class);

            messages = Arrays.asList(messagesItems);
            messages = new ArrayList<Message>(messages);
        } else
            return null;

        return (ArrayList<Message>) messages;
    }


    public static void remove(Context context) {
        Gson gson = new Gson();
        String jsonMessages = gson.toJson(new ArrayList<>());
        android.content.SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putString(MESSAGES, jsonMessages);

        editor.commit();
    }



}

