package com.dh.museum_moma.ui.chat;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dh.museum_moma.R;
import com.dh.museum_moma.communication.ServiceCallback;
import com.dh.museum_moma.communication.schema.Message;
import com.dh.museum_moma.communication.schema.Paint;
import com.dh.museum_moma.communication.schema.entities.EPaint;
import com.dh.museum_moma.controller.PaintController;
import com.dh.museum_moma.ui.detail.DetailActivity;
import com.dh.museum_moma.ui.home.HomeActivity;
import com.dh.museum_moma.util.GeneralContext;
import com.dh.museum_moma.util.SharedPreferencesApp;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;


public class ChatActivity extends AppCompatActivity implements ChatRecyclerViewAdapter.ItemClickListener {


    private RecyclerView recyclerView;
    private ChatRecyclerViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);

        getSupportActionBar().setTitle("");

        recyclerView = findViewById(R.id.recycler_view_chat);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ArrayList<Message> messages = SharedPreferencesApp.getMessages(GeneralContext.context);
        if (messages ==null){
            messages = new ArrayList<Message>();
            SharedPreferencesApp.saveMessages(GeneralContext.context,messages);
        }


        adapter = new ChatRecyclerViewAdapter(ChatActivity.this, (ArrayList<Message>) messages, (ChatRecyclerViewAdapter.ItemClickListener) ChatActivity.this);

        recyclerView.setAdapter(adapter);

        Button input = findViewById(R.id.input);

        EditText text = findViewById(R.id.text);

        input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message = new Message(text.getText().toString(), GeneralContext.getUserId());
                SharedPreferencesApp.saveMessage(GeneralContext.context,message);

                updateChatView();
            }
        });



    }

    private void updateChatView() {

        Intent intent = new Intent(ChatActivity.this, ChatActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onItemClick(Paint paint) {

    }

}
