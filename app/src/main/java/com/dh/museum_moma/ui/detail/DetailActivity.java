package com.dh.museum_moma.ui.detail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dh.museum_moma.R;
import com.dh.museum_moma.communication.schema.Artist;
import com.dh.museum_moma.communication.schema.Paint;
import com.dh.museum_moma.communication.schema.entities.EArtist;
import com.dh.museum_moma.communication.services.impl.ArtistRoomClient;
import com.dh.museum_moma.ui.home.HomeActivity;
import com.dh.museum_moma.util.GeneralContext;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity {

    private Paint mPaintSelected;
    private FirebaseDatabase database;
    private DatabaseReference referenceDatabase;
    private StorageReference mStorageReference;


    @BindView(R.id.nameArtistTextView)
    TextView nameArtistTextView;
    @BindView(R.id.nationalityTextView)
    TextView nationalityTextView;
    @BindView(R.id.namePaintTextView)
    TextView namePaintTextView;
    @BindView(R.id.influencedByTextView)
    TextView influencedByTextView;
    @BindView(R.id.imageDetail)
    ImageView imageDetail;

    private List<Artist> artists = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        mPaintSelected = (Paint) bundle.getSerializable(HomeActivity.PAINT_ARG);

        database = FirebaseDatabase.getInstance();
        referenceDatabase = database.getReference("artists");
        if (GeneralContext.isNetConnected()) {
            referenceDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    artists.clear();

                    for (DataSnapshot keyNode : dataSnapshot.getChildren()) {
                        Artist artist = keyNode.getValue(Artist.class);
                        artists.add(artist);
                    }

                    updateArtistDB( artists);
                    setData();

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } else {

            artists.clear();

            ArtistRoomClient artistRoomClient = new ArtistRoomClient();

            List<EArtist> eArtist = GeneralContext.getDB().artistRoomDao().getAllArtists();

            artists = mapEArtistToArtist(eArtist);

            setData();

        }

    }

    private void updateArtistDB(List<Artist> artistList) {
        for(int i=0 ; i < artistList.size(); i ++) {
            GeneralContext.getDB().artistRoomDao().insertEArtist(new EArtist(null, artistList.get(i).getArtistId(), artistList.get(i).getName(), artistList.get(i).getNationality(),artistList.get(i).getInfluencedBy()));
        }
    }

    private List<Artist> mapEArtistToArtist(List<EArtist> eArtist) {
        ArrayList<Artist> artists = new ArrayList<>();
        for (int i = 0 ; i< eArtist.size() ; i++){
            artists.add(new Artist(eArtist.get(i).getArtistId(),eArtist.get(i).getName(),eArtist.get(i).getNationality(),eArtist.get(i).getInfluencedby()));
        }
        return artists;
    }

    private void setData() {
        mStorageReference = FirebaseStorage.getInstance().getReference();

        StorageReference paintsRef = mStorageReference.child(mPaintSelected.getImage());

        Glide.with(imageDetail.getContext())
                .load(paintsRef)
                .into(imageDetail);

        namePaintTextView.setText(mPaintSelected.getName());

        Artist artist = getArtist(mPaintSelected.getArtistId());

        if (artist != null) {
            influencedByTextView.setText(artist.getInfluencedBy());
            nameArtistTextView.setText(artist.getName());
            nationalityTextView.setText(artist.getNationality());
        }
    }


    private Artist getArtist(String artistId) {
        for (int i = 0; i < this.artists.size(); i++) {
            if (artists.get(i).getArtistId().equals(artistId)) {
                return artists.get(i);
            }
        }
        return null;
    }


}
