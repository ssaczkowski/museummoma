package com.dh.museum_moma.ui.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dh.museum_moma.R;
import com.dh.museum_moma.communication.schema.Message;
import com.dh.museum_moma.communication.schema.Paint;
import com.dh.museum_moma.util.GeneralContext;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<ChatRecyclerViewAdapter.ViewHolder> {

    private List<Message> mValues;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private int isUSER= 1 ;


    public ChatRecyclerViewAdapter(Context context, List<Message> list, ItemClickListener itemClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mValues = list;
        mClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(viewType == isUSER){
             view = mInflater.inflate(R.layout.item_chat_right, parent, false);
        }else{

            view = mInflater.inflate(R.layout.item_chat_left, parent, false);
        }


        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        if ((mValues.get(position).getUser()).equals(GeneralContext.getUserId())) {
            return 1;

        } else {
            return 0;
        }
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String p = mValues.get(position).getText();
        holder.msgTextView.setText(p);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder  {
        TextView msgTextView;

        ViewHolder(final View itemView) {
            super(itemView);
            msgTextView = itemView.findViewById(R.id.msgTextView);

        }
    }

    Message getItem(int id) {
        return mValues.get(id);
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
        void onItemClick(Paint paint);
    }
}