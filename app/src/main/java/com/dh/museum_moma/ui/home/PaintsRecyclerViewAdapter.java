package com.dh.museum_moma.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dh.museum_moma.R;
import com.dh.museum_moma.communication.schema.Paint;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class PaintsRecyclerViewAdapter extends RecyclerView.Adapter<PaintsRecyclerViewAdapter.ViewHolder> {

    private List<Paint> mValues;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private StorageReference mStorageReference = FirebaseStorage.getInstance().getReference();


    public PaintsRecyclerViewAdapter(Context context, List<Paint> list, ItemClickListener itemClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mValues = list;
        mClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Paint p = mValues.get(position);
        holder.nameTextView.setText(p.getName());

        StorageReference paintsRef = mStorageReference.child(p.getImage());

        Glide.with(holder.itemView.getContext())
                .load(paintsRef)
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder  {
        TextView nameTextView;

        ImageView image;

        ViewHolder(final View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onItemClick(getItem(getAdapterPosition()));
                }
            });


            image = itemView.findViewById(R.id.image);
        }
    }

    Paint getItem(int id) {
        return mValues.get(id);
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
        void onItemClick(Paint paint);
    }
}