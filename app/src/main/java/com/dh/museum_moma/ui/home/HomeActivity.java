package com.dh.museum_moma.ui.home;


import android.content.Context;
import android.content.Intent;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dh.museum_moma.R;
import com.dh.museum_moma.communication.ServiceCallback;
import com.dh.museum_moma.communication.schema.Paint;
import com.dh.museum_moma.communication.schema.entities.EPaint;
import com.dh.museum_moma.controller.PaintController;
import com.dh.museum_moma.ui.chat.ChatActivity;
import com.dh.museum_moma.ui.detail.DetailActivity;
import com.dh.museum_moma.util.GeneralContext;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;


public class HomeActivity extends AppCompatActivity implements PaintsRecyclerViewAdapter.ItemClickListener {


    private RecyclerView recyclerView;

    public final static String PAINT_ARG = "mPaintSelected";

    private static final int CAMERA_REQUEST_CODE = 1;

    private StorageReference mStorage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        getSupportActionBar().setTitle("");

        PaintController paintController = new PaintController();

        mStorage = FirebaseStorage.getInstance().getReference();

        recyclerView = findViewById(R.id.recycler_view_home);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if(GeneralContext.isNetConnected()) {
            paintController.getPaint(new ServiceCallback.OnListTaskFinishListener() {
                @Override
                public void onSuccess(List dataList) {
                    PaintsRecyclerViewAdapter adapter;
                    adapter = new PaintsRecyclerViewAdapter(HomeActivity.this, (ArrayList<Paint>) dataList, (PaintsRecyclerViewAdapter.ItemClickListener) HomeActivity.this);

                    recyclerView.setAdapter(adapter);
                }

                @Override
                public void onError(String errorMsg) {

                }
            });
        }else{
            PaintsRecyclerViewAdapter adapter;
            adapter = new PaintsRecyclerViewAdapter(HomeActivity.this, getLocalPaintList(), (PaintsRecyclerViewAdapter.ItemClickListener) HomeActivity.this);

            recyclerView.setAdapter(adapter);
        }



    }

    private List<Paint> getLocalPaintList() {
        ArrayList<Paint> paintList = new ArrayList<>();


            for (EPaint ePaint :
                    GeneralContext.getDB().paintRoomDao().getAllPaints()) {
                paintList.add(new Paint(ePaint.getImage(),ePaint.getName(),ePaint.getArtistId()));
            }

                return paintList;
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onItemClick(Paint paint) {

        Intent intent = new Intent(this, DetailActivity.class);

        Bundle bundle = new Bundle();
        bundle.putSerializable(PAINT_ARG, paint);

        intent.putExtras(bundle);

        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_photo:
                newPhoto();
                return true;
            case R.id.log_out:
                closeSession();
                return true;
            case R.id.chat:
                openChat();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openChat() {
        Intent intent = new Intent(HomeActivity.this, ChatActivity.class);
        startActivity(intent);
    }

    private void closeSession() {
        LoginManager.getInstance().logOut();
        finish();
    }

    private void newPhoto() {


        final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri pictureUri= takePictureIntent.getData();
        takePictureIntent.putExtra("image",pictureUri);
        startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK){

            Bundle bundle = data.getExtras();
            Bitmap bitmap = (Bitmap) bundle.get("data");

            Uri uri = getImageUri(getApplicationContext(),bitmap);

            StorageReference filepath = mStorage.child("photos/").child(uri.getLastPathSegment());

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBitmap = baos.toByteArray();

            UploadTask uploadTask = filepath.putBytes(imageBitmap);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(HomeActivity.this,"System error, please try again. ",Toast.LENGTH_LONG).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Toast.makeText(HomeActivity.this,"Image successfully uploaded.",Toast.LENGTH_LONG).show();
                }
            });



        }

    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


}
