package com.dh.museum_moma.communication.services.dao;


import com.dh.museum_moma.communication.ServiceCallback;

public interface PaintDao {

    void getPaints(ServiceCallback.OnListTaskFinishListener listener);


}
