package com.dh.museum_moma.communication.services.impl;

import android.content.Context;
import android.content.res.AssetManager;

import com.dh.museum_moma.communication.schema.Paint;
import com.dh.museum_moma.communication.schema.PaintList;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class PaintRoomClient {

    public List<Paint> getPaints(Context context){
        List<Paint> paints = new ArrayList<>();

        AssetManager assetManager = context.getAssets();
        try {

            InputStream inputStream = assetManager.open("paints.json");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            Gson gson = new Gson();

            PaintList paintList = gson.fromJson(bufferedReader, PaintList.class);
            paints = paintList.getPaints();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return paints;

    }
}
