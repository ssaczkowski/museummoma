package com.dh.museum_moma.communication.services.impl;

import com.dh.museum_moma.communication.schema.Paint;
import com.dh.museum_moma.communication.schema.PaintList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PaintRetrofitService {

    @GET("bins/x858r")
    Call<PaintList> getAllPaints();

}
