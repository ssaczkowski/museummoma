package com.dh.museum_moma.communication.schema;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Artist {

    @SerializedName("artistId")
    @Expose
    private String artistId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("influencedby")
    @Expose
    private String influencedBy;

    public Artist(){

    }

    public Artist(String artistId, String name, String nationality, String influencedBy) {
        this.artistId = artistId;
        this.name = name;
        this.nationality = nationality;
        this.influencedBy = influencedBy;
    }

    public String getArtistId() {
        return artistId;
    }

    public String getName() {
        return name;
    }

    public String getNationality() {
        return nationality;
    }

    public String getInfluencedBy() {
        return influencedBy;
    }


}