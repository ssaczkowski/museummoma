package com.dh.museum_moma.communication;

import com.dh.museum_moma.communication.schema.PaintList;

import java.util.List;

import retrofit2.Response;

public class ServiceCallback {

   public interface OnListTaskFinishListener{
        void onSuccess(List dataList);
        void onError(String errorMsg);
    }

    public interface OnDataTaskFinishListener{
        void onSuccess(String data);
        void onError(String errorMsg);
    }

}
