package com.dh.museum_moma.communication.services.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.dh.museum_moma.communication.schema.entities.EArtist;

import java.util.List;

@Dao
public interface ArtistRoomDao {

    @Insert
    void insertEArtist(EArtist eArtist);

    @Query("SELECT * FROM eartist")
    List<EArtist> getAllArtists();

    @Query("select * from eartist where name = ':nameArtist'")
    List<EArtist> findById();




}

