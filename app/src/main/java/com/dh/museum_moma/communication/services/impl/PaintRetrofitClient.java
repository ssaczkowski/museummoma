package com.dh.museum_moma.communication.services.impl;

import com.dh.museum_moma.communication.ServiceCallback;
import com.dh.museum_moma.communication.schema.Paint;
import com.dh.museum_moma.communication.schema.PaintList;
import com.dh.museum_moma.communication.services.dao.PaintDao;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PaintRetrofitClient implements PaintDao {

    private Retrofit retrofit;
    private final String BASE_URL = "https://api.myjson.com/";
    private PaintRetrofitService paintRetrofitService;

    public PaintRetrofitClient() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.paintRetrofitService = this.retrofit.create(PaintRetrofitService.class);
    }

    @Override
    public void getPaints(final ServiceCallback.OnListTaskFinishListener listener) {

        Call<PaintList> call = paintRetrofitService.getAllPaints();

        call.enqueue(new Callback<PaintList>() {
            @Override
            public void onResponse(Call<PaintList> call, Response<PaintList> response) {
               PaintList paints = response.body();
                listener.onSuccess(paints.getPaints());
            }

            @Override
            public void onFailure(Call<PaintList> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        });

    }
}
