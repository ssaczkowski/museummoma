package com.dh.museum_moma.communication.schema.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity
public class EArtist {

    @PrimaryKey
    private Integer id;
    @ColumnInfo(name = "artistId")
    private String artistId;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "nationality")
    private String nationality;
    @ColumnInfo(name = "influencedby")
    private String influencedby;


    public EArtist(Integer id, String artistId, String name, String nationality,String influencedby) {
        this.id = id;
        this.name = name;
        this.artistId = artistId;
        this.nationality = nationality;
        this.influencedby = influencedby;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getInfluencedby() {
        return influencedby;
    }

    public void setInfluencedby(String influencedby) {
        this.influencedby = influencedby;
    }
}
