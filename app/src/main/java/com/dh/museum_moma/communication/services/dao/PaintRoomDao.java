package com.dh.museum_moma.communication.services.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.dh.museum_moma.communication.schema.entities.EPaint;

import java.util.List;

@Dao
public interface PaintRoomDao {

    @Insert
    void insertEPaint(EPaint ePaint);

    @Query("SELECT * FROM epaint")
    List<EPaint> getAllPaints();

    @Query("select * from epaint where name = ':namePaint'")
    List<EPaint> findById();




}

