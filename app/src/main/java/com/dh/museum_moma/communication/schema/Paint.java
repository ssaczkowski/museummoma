
package com.dh.museum_moma.communication.schema;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Paint implements Serializable {

    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("artistId")
    @Expose
    private String artistId;

    public Paint(String image, String name, String artistId) {
        this.image = image;
        this.name = name;
        this.artistId = artistId;
    }

    public String getImage() {
        return image;
    }


    public String getName() {
        return name;
    }

    public String getArtistId() {
        return artistId;
    }


}