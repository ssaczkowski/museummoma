package com.dh.museum_moma.communication.services.impl;

import android.content.Context;
import android.content.res.AssetManager;

import com.dh.museum_moma.communication.schema.Artist;
import com.dh.museum_moma.communication.schema.ArtistList;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class ArtistRoomClient {

    public List<Artist> getArtists(Context context){
        List<Artist> artists = new ArrayList<>();

        AssetManager assetManager = context.getAssets();
        try {

            InputStream inputStream = assetManager.open("artists.json");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            Gson gson = new Gson();

            ArtistList artistList = gson.fromJson(bufferedReader, ArtistList.class);
            artists = artistList.getArtists();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return artists;

    }
}
