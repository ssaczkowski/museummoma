package com.dh.museum_moma.communication.schema;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaintList {

    @SerializedName("paints")
    @Expose
    private List<Paint> paints = null;

    public List<Paint> getPaints() {
        return paints;
    }

    public void setPaints(List<Paint> paints) {
        this.paints = paints;
    }

}